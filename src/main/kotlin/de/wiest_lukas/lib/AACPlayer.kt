package de.wiest_lukas.lib

import net.sourceforge.jaad.aac.Decoder
import net.sourceforge.jaad.aac.SampleBuffer
import net.sourceforge.jaad.mp4.MP4Container
import net.sourceforge.jaad.mp4.MP4Exception
import net.sourceforge.jaad.mp4.api.AudioTrack
import java.io.File
import java.io.IOException
import java.io.RandomAccessFile
import java.util.logging.Logger
import javax.sound.sampled.AudioFormat
import javax.sound.sampled.AudioSystem
import javax.sound.sampled.LineUnavailableException
import kotlin.concurrent.thread


/**
 * This library plays AAC audio files, using the JAAD decoder. To use this lib,
 * you need to have the JAAD library jar, too.
 *
 * @author [Lukas Wiest](https://mastodontech.de/@9Lukas5)
 */
class AACPlayer {

    private var loop                = false
    private var repeat              = false
    private var playback: Thread?   = null
    private var paused              = false
    private var muted               = false
    private var interrupted         = false
    private var files               = mutableListOf<File>()
    private val logger              = Logger.getLogger(this.javaClass.packageName)

    /**
     * Creates a new Instance of AACPlayer with a set of Files to be played back.
     *
     * Non-existing Files or files with no tracks are ignored.
     *
     * @param files Filelist to playback.
     * @return New AACPlayer instance with the given files in given order as playlist
     */
    constructor(files: List<File>) {
        logger.finer("initializing AACPlayer")
        files.forEach { f ->
            try {
                val includedTracks = MP4Container(RandomAccessFile(f, "r"))
                    .movie
                    .tracks

                if (includedTracks.isNotEmpty()) {
                    this.files.add(f)
                    this.logger.finest("added to playlist: ${f.path}")
                }
                else this.logger.warning("no tracks found in ${f.name}. Skipping this one.")
            } catch (e: IOException) {
                this.logger.severe("FileNotFound, skipping ${f.name}")
            }
        }
    }

    /**
     * Player with only one File in List.
     * @param file
     */
    constructor(file: File): this(listOf(file))

    /**
     * Instances a new Player with one File, Path given as String.
     * @param pathToFile
     */
    constructor(pathToFile: String): this(File(pathToFile))

    private fun initThread() {
        interrupted = false
        playback = thread {

            try {
                var currentTrack = 0
                while (currentTrack in files.indices) {
                    logger.finer("loop control: currentTrack: $currentTrack")
                    logger.fine("current file: ${files[currentTrack].name}")
                    val track = MP4Container(RandomAccessFile(files[currentTrack], "r"))
                        .movie
                        .tracks
                        .filterIsInstance<AudioTrack>()
                        .firstOrNull()

                    if (track == null) throw MP4Exception("No audio tracks")
                    logger.finer("file has audio track")

                    val af = AudioFormat(
                        track.sampleRate.toFloat(),
                        track.sampleSize,
                        track.channelCount,
                        true,
                        true,
                    )

                    val line = AudioSystem.getSourceDataLine(af)
                    line.open()
                    line.start()
                    logger.finer("line opened and started")

                    val dec = Decoder(track.decoderSpecificInfo)
                    val buf = SampleBuffer()

                    logger.finer("starting playback")
                    playback@
                    while (!interrupted && track.hasMoreFrames()) {
                        logger.finest("frames available!")
                        dec.decodeFrame(track.readNextFrame().data, buf)
                        val b = buf.data
                        if (!muted) line.write(b, 0, b.size)

                        while (paused) {
                            logger.finest("playback is paused")
                            Thread.sleep(500)
                            if (interrupted) break@playback
                        }
                    }
                    logger.finer("playback finished")

                    line.drain()
                    line.close()
                    logger.finer("line drained & closed")

                    if (interrupted) return@thread

                    if (!loop) {
                        currentTrack++
                        logger.finer("loop control: increment currentTrack, new value: $currentTrack")
                    }
                    if (repeat) {
                        currentTrack %= files.size
                        logger.finer("loop control: modulo currentTrack, new value: $currentTrack")
                    }
                }
            } catch (e: Exception) {
                when(e) {
                    is LineUnavailableException,
                    is IOException,
                    is InterruptedException -> logger.severe("Playback was killed!\n${e.stackTraceToString()}")
                    else -> throw e
                }
            }
        }
    }

    /**
     * Blocking method, if a playback thread exists and hasn't ended yet.
     *
     * The playback thread will only be created, once the start function got called.
     * If no playback exists, this function will silently do nothing.
     *
     * @see play
     */
    fun join() = playback?.join()

    /**
     * Starts Playback of given File(s) with the first file.
     *
     * This function is non-blocking.
     * There are other functions available to get the current playing state or join the playback thread.
     *
     * @see isPlaying
     * @see join
     */
    fun play() {
        if (playback?.isAlive == true) {
            logger.warning("it plays yet, before you start again, stop it")
            return
        }

        initThread()
        logger.fine("playback thread initialized and started")
    }

    /**
     * Set stop-flag for the playback thread, signalling it to gracefully stop the playback.
     * To wait for the playback being done, you have to join the playback thread.
     *
     * @see join
     */
    fun stop() {
        interrupted = true
        logger.fine("set stop flag")
    }

    /**
     * Pauses playback.
     *
     * @see resume
     */
    fun pause() {
        paused = true
        logger.fine("set pause flag")
    }

    /**
     * Resumes playback on paused position.
     *
     * @see pause
     */
    fun resume() {
        paused = false
        logger.fine("unset pause flag")
    }

    /**
     * Toggle playback mute state
     *
     * This toggles the mute state on/off.
     *
     * If muted, the playback thread will continue to process the file,
     * but the audio data is not written to the open line anymore.
     *
     * @return true if player is muted after call
     *
     * @see isMuted
     */
    fun toggleMute(): Boolean {
        muted != muted
        logger.fine("toggled mute, new value: muted = $muted")
        return muted
    }

    /**
     * Enables loop of current file.
     * @see disableLoop
     */
    fun enableLoop() {
        loop = true
        logger.fine("enabled looping")
    }

    /**
     * Disables loop of current file.
     * @see enableLoop
     */
    fun disableLoop() {
        loop = false
        logger.fine("disabled looping")
    }

    /**
     * Enabled repeated playback of whole file list.
     * @see disableRepeat
     */
    fun enableRepeat() {
        repeat = true
        logger.fine("enabled repeat")
    }

    /**
     * Disables repeated playback of whole filelist.
     * @see enableRepeat
     */
    fun disableRepeat() {
        repeat = false
        logger.fine("disabled repeat")
    }

    /**
     * Checks the state of playback.
     *
     * @return true if a playback thread exists and is still alive
     *
     * @see play
     * @see stop
     * @see join
     */
    fun isPlaying() = playback?.isAlive?: false

    /**
     * Returns the mute state
     *
     * @return true if player is muted right now
     *
     * @see toggleMute
     */
    fun isMuted() = muted
}
