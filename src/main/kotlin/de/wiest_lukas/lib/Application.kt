package de.wiest_lukas.lib

import kotlinx.coroutines.Runnable
import picocli.CommandLine
import picocli.CommandLine.Command
import picocli.CommandLine.Option
import java.io.File
import java.util.logging.Level
import java.util.logging.Logger
import java.util.stream.Collectors

@Command(mixinStandardHelpOptions = true)
class Application: Runnable {
    @Option(
        names = ["-f", "--file"],
        description = ["File to play. This option can be given multiple times"],
        required = true,
    )
    private lateinit var files: List<String>

    @Option(
        names = ["-v", "--verbose"],
        description = ["Raise verbosity level with each occurrence"],
    )
    private lateinit var verbosity: BooleanArray

    override fun run() {
        val loglevel = when (if (!this::verbosity.isInitialized) 0 else verbosity.size) {
            0 -> Level.SEVERE
            1 -> Level.WARNING
            2 -> Level.INFO
            3 -> Level.CONFIG
            4 -> Level.FINE
            5 -> Level.FINER
            else -> Level.FINEST
        }
        val rootLogger = Logger.getLogger("")
        rootLogger.level = loglevel
        rootLogger.handlers.forEach { it.level = loglevel }

        val player = AACPlayer(files.stream()
            .map(::File)
            .collect(Collectors.toList()))
        player.play()
        player.join()
    }
}

fun main(args: Array<String>) {
    CommandLine(Application()).execute(*args)
}
