# Changelog

All notable changes to this project will be documented in this file. See [commit-and-tag-version](https://github.com/absolute-version/commit-and-tag-version) for commit guidelines.

## [1.4.0](../../compare/1.3.2...1.4.0) (2024-04-28)


### Features

* port to kotlin ([775e7d3](775e7d35635ecca407a8ccee927a82e7c842b38d))

### [1.3.2](https://gitlab.com/9Lukas5/java-aac-player/-/compare/1.3.1...1.3.2) (2022-01-14)


### Features

* **pom:** remove maven gpg and nexus staging plugins from dependencies ([2b1f29f](https://gitlab.com/9Lukas5/java-aac-player/-/commit/2b1f29f395bb2cb866250a87fadd8195600e071b))


### Bug Fixes

* move to own published jaad artifact, which has the crashing commit reverted ([9e0be28](https://gitlab.com/9Lukas5/java-aac-player/-/commit/9e0be28ba36210e1927d7ec3371edc2a17f8698a))

### [1.3.1](https://gitlab.com/9Lukas5/java-aac-player/-/compare/1.3.0...1.3.1) (2022-01-13)


### Bug Fixes

* **ci:** make CI_COMMIT_TAG available as BUILD_NUMBER ([3e62bf5](https://gitlab.com/9Lukas5/java-aac-player/-/commit/3e62bf5d6e3b0705b7d0bcba73c6e09cd5bbfc52))

## [1.3.0](https://gitlab.com/9Lukas5/java-aac-player/-/compare/1.2.1...1.3.0) (2022-01-13)


### Features

* refactor to mvn project ([6e66b8c](https://gitlab.com/9Lukas5/java-aac-player/-/commit/6e66b8c564fa7e023e0e6b03b983aed3aa50db5f))


### Bug Fixes

* ClassCastException on file with videotrack ([c0912c0](https://gitlab.com/9Lukas5/java-aac-player/-/commit/c0912c0fe776152296d9644e3f10dce04d8b94f3))
