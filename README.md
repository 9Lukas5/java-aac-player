# Java AAC Player

This player is a wrapper around the JAAD library to provide an easy to use
interface to playback aac audio files, stored in Mpeg4 containers.

## Publishes from this repo

Publishes from this repo will be available in the Maven Central repository under

```xml
<dependency>
    <groupId>com.gitlab.9lukas5</groupId>
    <artifactId>java-aac-player</artifactId>
    <!--
        check latest version on https://search.maven.org
        or try the latest tag you find in git
    -->
</dependency>
```

## Usage

A Basic usage example would like this:

=== "Kotlin"

    ```kotlin
    val player = AACPlayer("audio.m4a") // create player instance
    player.play()                       // start playback
    player.join()                       // wait for playback to finish
    ```

=== "Java"

    ```java
    AACPlayer player = new AACPlayer("audio.m4a");  // create player instance
    player.play();                                  // start playback
    player.join();                                  // wait for playback to finish
    ```
